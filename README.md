The main functionality of this python script is to detect orange and green cones
into the pictures captured by an USB camera. It's prepared to work in a 
RaspBerryPi 3 model B with a Raspian OS installed.

-**Cone detection**:

The code is prepared to detect the biggest orange and green contourns of the
image. Then it calculates the middle point between them.

-**Data sending**:

    DDS: This python script was developed in a project where communication among
        modules used DDS. The relevant position data of the cones 
        (X,Y coordinates of the cones and middle point between them)
        is being sent to other modules using this standard.
        
    TCP: This script also send the JPEG encoded images captured by the camera to
        a computer or server hosted in the same local network (You may change 
        the IP address in the code).