#!/usr/bin/env python
import cv2
import numpy as np
import imutils
import time
import subprocess
import socket

def detect(c):
    shape="unidentified"
    peri = cv2.arcLength(c, True)
    approx = cv2.approxPolyDP(c, 0.04 * peri, True)
#    print(len(approx))

def findLargestContours(contours):
    c1 = max(contours, key = cv2.contourArea)
    cnts = []
    cnts.append(c1)
    return cnts
t=1
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FPS, 10)
pipe = subprocess.Popen(['./opencv_dds'],stdin=subprocess.PIPE,stdout=subprocess.PIPE)
first = 1
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(("192.168.137.247", 80))
while(True):
    #time.sleep(t)
    # Take each frame
    _, frame = cap.read()
    img_bytes = cv2.imencode('.jpg',frame)[1].tobytes()
    s.write(25)
    s.write(img_bytes)
    #Take size of image
 #   print(frame.shape[0])
    height,width,_=frame.shape
    centerX = width/2
    centerY = height/2
    msg = str(centerX)+','+str(centerY)+';'
    #print(centerX," ",centerY)

    # Convert BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # define range of orange color in HSV
    lower_orange = np.array([6,200,145])
    upper_orange = np.array([22,255,255])

    lower_green = np.array([32,41,115])
    upper_green = np.array([77,255,255])

    # Threshold the HSV image to get only orange colors
    mask_orange = cv2.inRange(hsv, lower_orange, upper_orange)
    mask_green = cv2.inRange(hsv, lower_green, upper_green)

    # Bitwise-AND mask and original image
    res_orange = cv2.bitwise_and(frame,frame, mask= mask_orange)
    res_green = cv2.bitwise_and(frame,frame, mask= mask_green)

    #res_final = cv2.add(res_orange,res_green)

    #res_final_resized = imutils.resize(res_final, width=300)

    res_orange_resized = imutils.resize(res_orange, width=300)
    res_green_resized = imutils.resize(res_green, width=300)

    ratio_orange = frame.shape[0] / float(res_orange_resized.shape[0])

    ratio_green = frame.shape[0] / float(res_green_resized.shape[0])

    res_orange_resized_gray = cv2.cvtColor(cv2.cvtColor(res_orange_resized,cv2.COLOR_HSV2BGR),cv2.COLOR_BGR2GRAY)
    res_green_resized_gray = cv2.cvtColor(cv2.cvtColor(res_green_resized,cv2.COLOR_HSV2BGR),cv2.COLOR_BGR2GRAY)

    blurred_orange = cv2.GaussianBlur(res_orange_resized_gray,(5,5),0)
    thresh_orange = cv2.threshold(blurred_orange, 60, 255, cv2.THRESH_BINARY)[1]

    blurred_green = cv2.GaussianBlur(res_green_resized_gray,(5,5),0)
    thresh_green = cv2.threshold(blurred_green, 80, 255, cv2.THRESH_BINARY)[1]

    cnts_orange = cv2.findContours(thresh_orange.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts_orange = cnts_orange[0] if imutils.is_cv2() else cnts_orange[1]

    cnts_green = cv2.findContours(thresh_green.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts_green = cnts_green[0] if imutils.is_cv2() else cnts_green[1]

    cX_orange=0
    cY_orange=0

    cX_green=0
    cY_green=0

    avg_cX_orange=0
    avg_cX_green=0
    avg_cY_orange=0
    avg_cY_green=0

    if len(cnts_orange) > 1:
        cnts_orange = findLargestContours(cnts_orange)

    for c in cnts_orange:
        M = cv2.moments(c)
        if M["m00"] != 0:
            detect(c)
            cX_orange += int((M["m10"] / M["m00"]) * ratio_orange)
            cY_orange += int((M["m01"] / M["m00"]) * ratio_orange)
        c=c.astype("float")
        c = c*ratio_orange
        c=c.astype("int")
        cv2.drawContours(frame, [c], -1, (0, 255, 0), 2)

    if len(cnts_orange) != 0:
        avg_cX_orange = cX_orange/len(cnts_orange)
        avg_cY_orange = cY_orange/len(cnts_orange)

    if len(cnts_green) > 1:
        cnts_green = findLargestContours(cnts_green)

    for c in cnts_green:
        M = cv2.moments(c)
        if M["m00"] != 0:
            detect(c)
            cX_green += int((M["m10"] / M["m00"]) * ratio_green)
            cY_green += int((M["m01"] / M["m00"]) * ratio_green)
        c=c.astype("float")
        c = c*ratio_green
        c=c.astype("int")
        cv2.drawContours(frame, [c], -1, (0, 255, 0), 2)

    if len(cnts_green) != 0:
        avg_cX_green = cX_green/len(cnts_green)
        avg_cY_green = cY_green/len(cnts_green)
    msg+=str(avg_cX_orange)+','+str(avg_cY_orange)+';'+str(avg_cX_green)+','+str(avg_cY_green)+'\n'
    avg_X = (avg_cX_orange + avg_cX_green)/2
    avg_Y = (avg_cY_orange + avg_cY_green)/2
    print(msg)
   # print("AVERAGE POINT: [",avg_X,",",avg_Y,"]")
    pipe.stdin.write(msg.encode())
    cv2.circle(frame,(int(avg_X),int(avg_Y)), 10, (0,0,255), -1)
    cv2.circle(frame,(int(centerX),int(centerY)), 10, (115,15,165), -1)
    if first == 1:
	print('ok')
	first=0
 # cv2.imshow('frame',frame)
  #  cv2.imshow('mask_orange',mask_orange)
   # cv2.imshow('res_orange',res_orange)
    #cv2.imshow('res_green',res_green)
    #cv2.imshow('res_final',res_final)

    #k = cv2.waitKey(5) & 0xFF
    #if k == 27:
     #   break

cv2.destroyAllWindows()